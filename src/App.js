import './App.css';
import PokemonList from './pokemon';
function App() {
  return (
    <div className="App">
      <h1>Pokemon - Liste</h1>
      <h2>Filter Types</h2>
        <PokemonList/>
    </div>
  );
}

export default App;
