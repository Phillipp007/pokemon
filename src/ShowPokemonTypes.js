import React, { useState } from 'react'
export default function ShowPokemonTypes({typeList,parentCallback}) {
    
      function filterPokemonType(changeType,isChecked) {
        typeList.filter(type =>type.name === changeType).map((type) => {
            type.isNotFiltered = !isChecked
        })
        const newFilter = typeList.filter(type =>!type.isNotFiltered) 
        parentCallback(newFilter);
      }
      

      return (
            <div  class="btn-group" role="group" aria-label="Basic checkbox toggle button group">
            {    
                    typeList.map((type,index) => (
                        <div>                            
                            <input type="checkbox" class="btn-check" id={type.name+index} autocomplete="off" onChange={(e) => filterPokemonType(type.name,e.target.checked)}/>   
                            <label class="btn btn-outline-primary" for={type.name+index}>{type.name}</label>
                        </div>
                    ))
            }
            </div>
      )
}