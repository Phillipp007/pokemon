import React, { useState, useEffect } from 'react'
import axios from 'axios'
import ShowPokemonTypes from './ShowPokemonTypes'

export default function FilterTypes({parentCallback}) {
    
    var [typeList,setTypeList] = useState([])

    const getData = () => {
        let apiLink = 'https://pokeapi.co/api/v2/type/';
            axios.get(apiLink)
            .then(res=>{
                Array.apply(null, { length:res.data.count }).map((e, i) => (
                    setTypeList(typeList => [...typeList, {name:res.data.results[i].name,isNotFiltered:true}])
                ))
                
            })
            .catch(err=>{
                console.log(err);
            })
    }

    useEffect(() => {
        getData()
      }, [])      

      return <ShowPokemonTypes typeList={typeList} parentCallback = {parentCallback}/>
}