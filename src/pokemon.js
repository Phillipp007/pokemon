import React, { useState, useEffect } from 'react'
import axios from 'axios';
import ShowPokemon from './showPokemon';
import FilterTypes from './FilterTypes';

export default function GetPokemon() {
    const [pokemonList,setPokemonList] = useState([])

    const [filterTypes,setFilterTypes] = useState([])
    const n = 150;

    const getData = () => {
        let apiLink = 'https://pokeapi.co/api/v2/pokemon/';
        Array.apply(null, { length: n }).map((e, i) => (
            axios.get(apiLink+(i+1))
            .then(res=>{
              if (filterPokemon(res.data.types)){
                setPokemonList(pokemonList => [...pokemonList,
                 {name:res.data.name,id:res.data.id,pictureUrl:res.data.sprites.front_default}]);
              }
            })
            .catch(err=>{
                console.log(err);
            })
          ));

    }

    function handleCallBack(filterTypes){
        setPokemonList([])
        setFilterTypes(filterTypes)
        //getData()
    }

    function filterPokemon(types){
      if (filterTypes.length==0){
        return true
      }
      for (let type of types) {
        for (let filter of filterTypes){
          if (filter.name==type.type.name){
            return true;
          }
        }
      }
      return false;
    }
  
    useEffect(() => {
      getData()
    },[])

    function sortPokemonList(){
      pokemonList.sort(function(a, b) {
          return parseFloat(a.id) - parseFloat(b.id);
      });
      return pokemonList
    }

    return (
      <div>
        <FilterTypes parentCallback={handleCallBack} />
        <ShowPokemon  pokemonList={sortPokemonList()}/>
      </div>
    );
  }

 