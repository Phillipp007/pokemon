import React, { useState, useEffect } from 'react'

export default function ShowPokemon({pokemonList}) {

    return (
        <div>
            <ul class="list-group list-group-flush">
              {
                pokemonList.map((pokemon, index) => (
                    // Setting "index" as key because name and age can be repeated, It will be better if you assign uniqe id as key
                    <li class="list-group-item" key={index}>
                        <span>ID: {pokemon.id}</span>{" "}
                        <span>Name: {pokemon.name}</span>{" "}
                        <span><img src={pokemon.pictureUrl} alt={pokemon.name}/></span>
                    </li>
                ))
              }
            </ul>
          </div>
      )

  }

 